import React, { useEffect, useState } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import './App.css';
import Dashboard from './components/Dashboard';
import { data } from './mock';
import CheckOutCart from "./pages/CheckOutCart";
import ProfilePage from './pages/ProfilePage';
import { Icourse } from './types';

function App() {
//adding states

const [allcourses, setallcourses] = useState<Icourse[]>(data);
const allcoursesHandler = (newState: Icourse[]) => {
  setallcourses( newState)
}

let [sortType, setsortType] = useState<string>("low_to_high");
const sortTypeHandler = (type: string) => {
  setsortType(type)
}
const sort = () => {
  const newState: Icourse[] = allcourses;
  if (sortType === "low_to_high") {
    newState.sort((a: Icourse, b: Icourse) => a.price <= b.price ? 1 : -1)
    allcoursesHandler(newState)
  }
  else if (sortType === "high_to_low") {
    newState.sort((a: Icourse, b: Icourse) => a.price >= b.price ? 1 : -1)
    allcoursesHandler(newState)
  }
}
useEffect(() => {
 sort();
}, [sortType])






//selected courses
let [selectedCourses, setselectedCourses] = useState<Icourse[]>([]);

useEffect(() => {
  setselectedCourses(selectedCourses)

}, [selectedCourses])

let [amount, setamount] = useState<number>(0);

const selectHandler = (course: Icourse) => {
  let newSelectedState: Icourse[] = selectedCourses;
  newSelectedState.push(course);
  setselectedCourses(newSelectedState)
  let tempamount = 0;
  selectedCourses.map((course: Icourse) => { tempamount += course.price })
  setamount(tempamount)
  
}

const deleteHandler = (course: Icourse) => {
  let newSelectedState: Icourse[] = selectedCourses;
   let index= newSelectedState.indexOf(course);
   if(index>-1)
   {
     newSelectedState.splice(index,1);
   }
  setselectedCourses(newSelectedState)
  let tempamount = 0;
  selectedCourses.map((course: Icourse) => { tempamount += course.price })
  setamount(tempamount)
}

let [addflag, setaddflag] = useState(true);






  return (
    <Switch>
            <Route exact path="/" render={()=><Dashboard allcoursesHandler={allcoursesHandler} courses={allcourses} amount={amount} selectedCourses={selectedCourses} filterHandle={sortTypeHandler} selectHandler={selectHandler} addflag={addflag} deleteHandler={deleteHandler}/>} />
            <Route exact path="/checkOutpage"  render={()=><CheckOutCart courses={allcourses} addflag={addflag} deleteHandler={deleteHandler} selectedCourses={selectedCourses} amount={amount} selectHandler={selectHandler}/>}/> 
            <Route exact path="/myProfile" component={ProfilePage}/>
            
            
        </Switch>
  );
}

export default App;
