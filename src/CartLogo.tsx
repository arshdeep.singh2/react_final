import React from 'react';
import { Link } from 'react-router-dom';
import cartlogo from './images/shopping-cart.svg';
export default function CartLogo() {
    return (
        <Link to ="/checkOutpage" id ="cart-logo">
            <img src={cartlogo}></img>
        </Link>
    )
}
