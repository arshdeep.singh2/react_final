export interface Icourse{
        id:number; 
        title:string;
        author:string;
        price:number;
        description:string;
        tags: string[];

}