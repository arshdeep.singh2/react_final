import React from 'react'
import { Link } from 'react-router-dom'
import MainHeader from '../components/MainHeader'
import TopNav from '../components/TopNav'
import user from "../images/user.png"
export default function ProfilePage() {
    return (
        <main>
            <TopNav></TopNav>
            <MainHeader tagline={"My Profile"}></MainHeader>
            <img src ={user}></img>
            <div className="form-container">
                <form>
                    <label >
                        Display Name
                    </label>
                    <input id ="place" name="display-name" type="text"/>
                    <label >
                        First Name
                    </label>
                    <input id ="place" name="fname" type="text"/>
                    <label >
                        Second Name
                    </label>
                    <input id ="place" name="sname" type="text"/><br/>
                    <label>About Yourself</label><br/>
                    <input name="yourself" type="text" id ="yourself"></input><br/>
                    <div>
                    <div>Your Areas of Interest</div><br/>
                    <label>
                        Developer
                        <input type="checkbox"></input>
                    </label><br/><br/>
                    <label>
                        Designer
                        <input type="checkbox"></input>
                    </label><br/><br/>
                    <label>
                        Project Manager
                        <input type="checkbox"></input>
                    </label><br/><br/>
                    <label>
                        Sales
                        <input type="checkbox"></input>
                    </label>
                    </div>
                     <br/><br/>
                     <div>
                     <div>Are You Student or Professional</div><br/>
                    <label>
                        Student
                        <input type="radio"></input>
                    </label><br/><br/>
                    <label>
                        Professional
                        <input type="radio"></input>
                    </label><br/><br/> </div>
                    <div>What is your Experience </div><br/>
                    <label>
                        0-5
                        <input type="radio"></input>
                    </label><br/><br/>
                    <label>
                        5-10
                        <input type="radio"></input>
                    </label><br/><br/>
                    <label>
                        10 and Above
                        <input type="radio"></input>
                    </label><br/>

                   <Link to ="/"> <input id="submit" type="submit" value="SAVE" /></Link>
                </form>
            </div>


        </main>
    )
}
