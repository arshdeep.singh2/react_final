import React, { useState } from 'react'

import '../App.css';
import TopNav from '../components/TopNav';
import {Icourse} from '../types';
import {useLocation} from 'react-router-dom';
import MainHeader from '../components/MainHeader';
import CourseCard from '../components/CourseCard';
import Modal from "../components/modal";
export default function CheckOutCart(props:{courses:Icourse[];selectedCourses:Icourse[]; amount:number;selectHandler:(course:Icourse)=>void;addflag:boolean;deleteHandler:(course:Icourse)=>void}) {
    const location = useLocation().state;
    const [coutmodal, setcoutmodal] = useState(false)
    const modalHandler =()=>{
      setcoutmodal(!coutmodal);
    } 
    const clickHandler=()=>{
      modalHandler();
    }
    const selectedCourses=location;
    console.log("kjfkje",selectedCourses);
    
    return (
        <div>
           <TopNav /> 
           <main>
            <MainHeader tagline="Shopping Cart" />
            <div id ="course-container">
            <h4>Selected Courses</h4>
            {props.selectedCourses.map((course:Icourse)=>{return (<CourseCard selectedCourses={props.selectedCourses} course={course} selectHandler={props.selectHandler} addflag={false} deleteHandler={props.deleteHandler}/>)})}
            
              <h4>Recommended Courses</h4>
              {props.courses.map((course:Icourse)=>{return (<CourseCard selectedCourses={props.selectedCourses} course={course} selectHandler={props.selectHandler} addflag={true} deleteHandler={props.deleteHandler}/>)})}
            </div>
            <div id ="cart-container">
            
              <h2>Your Cart Details</h2>
        
              <div id="amount-cart">
                Total Cart Value
               <h2>Rs {props.amount} /-</h2>
              </div>
              <Modal errormsg={"Your order placed successfully"} btnmsg="ok" show={coutmodal} modalHandler={modalHandler} link={true}/>
              <button id="checkout-cart" onClick={clickHandler}>
                  CHECKOUT
              </button>
        
            </div>
        </main>
            
        </div>
    )
}
