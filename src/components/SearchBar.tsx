import React, { useEffect, useState } from 'react'
import { Icourse } from '../types';
import Modal from "./modal";
export default function SearchBar(props:{allcourses:Icourse[];allcoursesHandler:(courses:Icourse[])=>void}) {
    const [keywords, setkeywords] = useState<string>("");
    const [modal, setmodal] = useState(false)
     const searchcourses =()=>{
        let newstate:Icourse[]=[];
         props.allcourses.forEach((course:Icourse)=>{
             if(keywords.toLowerCase()===course.tags[0].toLowerCase())
             {
                 newstate.push(course);
             }
         })
        
        if(newstate.length===0)
        {
           setmodal(true);
        }
        else{
            props.allcoursesHandler(newstate);
        }
     }
     const modalhandler=()=>{
         setmodal(!modal);
     }
    return (
        <div id ="search-bar">
            <input id="search-input"
             value={keywords}
             placeholder={"search courses"}
             onChange={(e)=>setkeywords(e.target.value)}
             onKeyPress={event => {
                    if (event.key === 'Enter') {
                      searchcourses();
                    }
                  }}
            />
            <Modal link={true} errormsg={"No Related course found"} btnmsg={"close"} show={modal} modalHandler={modalhandler}/>
        </div>
    )
}
