import React from 'react'

export default function modal(props:{link?:boolean;errormsg:string;btnmsg:string; show:boolean;modalHandler:()=>void}) {
    function refreshPage() {
        props.modalHandler();
        if(props.link===true)
        {
        window.location.reload(false);
        }
      }
  
  
    if(props.show){
    return (
        <div id="modal-container">
        <div id ="modal">
            <h3>{props.errormsg}</h3><br/><br/>
            <button onClick={refreshPage}>{props.btnmsg}</button>
        </div>
        </div>
    )}
    else{
        return null;
    }
}
