import React, { useEffect, useState } from 'react';
import MainHeader from './MainHeader';
import CourseCont from './containers/CourseCont';
import { Route, Switch } from 'react-router-dom';
import CartCont from './containers/CartCont';
import {data} from "../mock";
import {Icourse} from "../types";
import Dashboard from './Dashboard';
import CheckOutCart from '../pages/CheckOutCart';

export default function Main(props :{courses :Icourse[];amount: number, selectedCourses: Icourse[] ;filterHandle:(sortType:string)=>void;selectHandler:(course:Icourse)=>void;addflag:boolean;deleteHandler:(course:Icourse)=>void;allcoursesHandler:(courses:Icourse[])=>void}) {

    return (
      
        <main>
            <MainHeader tagline="Discover Latest Course on React" />
            <CourseCont allcoursesHandler={props.allcoursesHandler} selectedCourses={props.selectedCourses} courses={props.courses} filterHandle={props.filterHandle} selectHandler={props.selectHandler} addflag={props.addflag} deleteHandler={props.deleteHandler}/>
            <CartCont amount={props.amount} selectedCourses={props.selectedCourses}/>
        </main>
        
    )
}
