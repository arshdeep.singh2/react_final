import React from 'react';

export default function TextCol(props:{tagline:string}) {
    return (
        <div id= "text-main-header">
            {props.tagline}
        </div>
    )
}
