import React from 'react'
import TextCol from "./TextCol";
import Imgcol from "./ImgCol";
export default function MainHeader(props:{tagline:string}) {
    return (
        <div id ="main-header">
            <TextCol tagline={props.tagline}/>
            <Imgcol />
        </div>
    )
}
