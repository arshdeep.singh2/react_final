import React from 'react'
import logo from "../images/shopping-cart.svg"
export default function CartLogo() {
    return (
        <div id="cart-logo" >
            <img src={logo}></img>
        </div>
    )
}
