import React, { useEffect, useState } from 'react'

export default function SortBtn(props:{filterHandle:(sortType:string)=>void}) {
  const [val, setval] = useState("low_to_high"); 
  const formHandler = ()=>{
  props.filterHandle(val)
  }
  useEffect(() => {
    formHandler();
  }, [val])

    return (
       <form id ="myForm">
       <select name="sortType" id="sort-btn" onChange={(e)=>setval(e.target.value)} >
         <option value ="" >Course Price</option>
         <option value="low_to_high">Low to High</option>
         <option value ="high_to_low" >High to Low</option>
       </select>
       </form>
    )
}
