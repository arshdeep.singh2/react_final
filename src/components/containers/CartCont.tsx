import React, { useEffect, useState } from 'react'
import CartCourseCard from "../CartCourseCard";
import {Icourse} from "../../types"; 
import {Link} from "react-router-dom";
export default function CartCont(props: { amount: number, selectedCourses: Icourse[] }) {
  
  if (props.amount === 0) {
    return (
      <div id="cart-container">
        <h2>Your Cart Details</h2>
        <i>Your cart is empty right now Please add courses in the cart from list</i>
        <div id="amount">
          Total Cart Value
            <h2>Rs {props.amount} /-</h2>
        </div>
      </div>
    )
  }
  else {
    return (
      <div id="cart-container">
        <h2>Your Cart Details</h2>
        <div id="cart-course-card">
          {props.selectedCourses.map((course: Icourse) => {
            return (<CartCourseCard course={course} />)
          })}
        </div>
        <div id="amount">
          Total Cart Value
            <h2>Rs {props.amount} /-</h2>
        </div>
        <div id="checkout-btn">
          <Link to="/checkOutPage"
          > GO TO CHECKOUT  </Link>
        </div>
      </div>
    )
  }

}
