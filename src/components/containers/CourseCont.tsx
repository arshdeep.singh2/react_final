import React from 'react';
import Bar from "../Bar";
import {Icourse} from "../../types";
import CourseCard from "../CourseCard";

export default function CourseCont(props :{courses :Icourse[];selectedCourses :Icourse[] ;filterHandle:(sortType:string)=>void;selectHandler:(course:Icourse)=>void;addflag:boolean;deleteHandler:(course:Icourse)=>void;allcoursesHandler:(courses:Icourse[])=>void}) {
    const courses:Icourse[] = props.courses;
    console.log(courses);
    return (
        <div>
           <Bar allcourses={props.courses} allcoursesHandler={props.allcoursesHandler} filterHandle={props.filterHandle} />
        <div id ="course-container">
            {
              courses.map((course,index)=>{
              return (<CourseCard selectedCourses={props.selectedCourses} course={course} selectHandler={props.selectHandler} addflag={props.addflag} deleteHandler={props.deleteHandler}/>)
               
              })
            }
        </div>
        </div>
    )
}
