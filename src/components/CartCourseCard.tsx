import React from 'react'
import {Icourse} from "../types";
export default function CartCourseCard(props:{course:Icourse}) {
    return (
        <div>
          <h6>{props.course.title}   Rs {props.course.price}</h6>  
        </div>
    )
}
