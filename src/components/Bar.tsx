import React from 'react'
import Display from "./Display";
import SortBtn from "./SortBtn";
import SearchBar from './SearchBar';
import { Icourse } from '../types';
export default function Bar(props:{allcourses:Icourse[];filterHandle:(sortType:string)=>void;allcoursesHandler:(courses:Icourse[])=>void}) {
    return (<div>
        <div id ="bar">
           <Display/> 
           <SortBtn filterHandle={props.filterHandle}/>
        </div>
        <div>
          <SearchBar allcourses={props.allcourses} allcoursesHandler={props.allcoursesHandler}/>
        </div>
        </div>
    )
}
