import React, { useState } from 'react'
import {Icourse} from "../types";
import Modal from "./modal"
export default function CourseCard(props:{selectedCourses:Icourse[] ;course:Icourse; selectHandler:(course:Icourse)=>void;addflag:boolean;deleteHandler:(course:Icourse)=>void}) {
    const course:Icourse =props.course;
    const [modal, setmodal] = useState(false)
    const modalHandler =()=>{
       setmodal(!modal)
    }


    const clickhandler =()=>{
      
        if(props.addflag)
        { 
          let go=true;
          props.selectedCourses.forEach((c:Icourse)=>{
            if(c.id===props.course.id)
            {
              go=false;
            }
          }) 


          if(go)
         {   
          props.selectHandler(props.course)
         }
         else
         {
          modalHandler();
         }
        }
        else
        {
          props.deleteHandler(props.course)
        }
    }

    const helper=()=>{
      if(props.addflag)
           {
            return <button id="addcart-btn" onClick={clickhandler}>ADD TO CART</button>
           }
           else
           {
             return <button id ="addcart-btn" onClick={clickhandler}>DELETE</button>
           }

    }
    return (
        <div id ="course-card">
          <div id="block"></div>
          <div id= "title-block">
              <div>{course.title}</div>
              <div id = "tag">{course.tags[0]}</div><div id = "tag">{course.tags[1]}</div>
          </div>
          <div id ="author">
            {course.author}
          </div>
          <div id ="price">
           <h3>Rs {course.price}</h3>
          </div>
          {helper()}
          <Modal errormsg="Already in Cart" btnmsg="close" show={modal} modalHandler={modalHandler}/>
        </div>
    )
}
