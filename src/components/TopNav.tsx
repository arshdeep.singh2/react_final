import React from 'react';
import HashedinLogo from './HashedinLogo';
import CoursesLogo from './CoursesLogo';
import MyWhishListLogo from './MyWhishListLogo';
import CartLogo from './CartLogo';
import { Link } from 'react-router-dom';
import Profile from './Profile';
export default function TopNav() {
    return (
        <header>
            
            <Link to ="/"><HashedinLogo /></Link>
            <Link to ="/"><CoursesLogo/></Link>
             <Link to ="/"><MyWhishListLogo/></Link>    
            <Link to ="/checkOutpage"> <CartLogo /> </Link>
             <Link to="/myProfile"><Profile/></Link>
        </header>

    )
}
