import React from 'react';
import TopNav from './TopNav';
import Main from './Main';
import { Link } from 'react-router-dom';
import { Icourse } from '../types';

export default function Dashboard(props :{courses :Icourse[];amount: number, selectedCourses: Icourse[] ;filterHandle:(sortType:string)=>void;selectHandler:(course:Icourse)=>void;addflag:boolean;deleteHandler:(course:Icourse)=>void;allcoursesHandler:(courses:Icourse[])=>void}) {
    return (
        <div>
            <TopNav /> 
            <Main allcoursesHandler={props.allcoursesHandler} amount={props.amount} courses={props.courses} filterHandle={props.filterHandle} selectedCourses={props.selectedCourses} selectHandler={props.selectHandler} addflag={props.addflag} deleteHandler={props.deleteHandler}/>
        </div>
        
    )
}
